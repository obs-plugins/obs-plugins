"""
Notify Telegram script

This script is used to send a notification in Telegram channel when a stream is started in OBS studio

Note: install pyTelegramBotAPI libraries first
- pyTelegramBotAPI - https://github.com/eternnoir/pyTelegramBotAPI

==========================================================

EXAMPLES

Variables file structure:
"VAR1","VALUE"
"VAR2","VALUE, 2"

This will be parsed as (without quotes):
VAR1 -> 'VALUE'
VAR2 -> 'VALUE, 2'
"""

# Imports
# =======

# STD Library
import csv
import threading
from collections import namedtuple

# OBS Library
# noinspection PyUnresolvedReferences
import obspython as obs

# Telegram Bot Library
import telebot

# Constants
# =========

DEFINED_CONSTANT_ID_TYPE = namedtuple('DEFINED_CONSTANTS_TYPE', [
    'source_name__message_path',
    'source_name__variables_path',
    'source_name__telegram_bot_token',
    'source_name__telegram_bot_channel_id',
    'button_name__send_test_message',
])

CONSTANT_ID = DEFINED_CONSTANT_ID_TYPE(
    'source__message_path',
    'source__variables_path',
    'source__telegram_bot_token',
    'source__telegram_bot_channel_id',
    'button__send_test_message',
)


# Global variables
# ================

message_template_file_path = ""
template_variables_file_path = ""
telegram_bot_token = ""
telegram_bot_channel_id = ""


# Custom functions
# ================

def send_notification(props, prop):
    thread = threading.Thread(target=send_notification_execute, name="Send TG message")
    thread.name = "Send TG message"
    thread.daemon = True
    thread.start()


def send_notification_execute():
    print('Telegram message - start sending notification')

    # Format message text
    message_template: str
    with open(message_template_file_path, 'r', encoding="utf-8") as message_template_file:
        message_template = ''.join(message_template_file.readlines())

    # Get variables
    variables = dict()
    with open(template_variables_file_path, 'r', encoding="utf-8") as variables_file:
        variables_raw = variables_file.readlines()
        variables_raw = list(map(lambda x: x[:-1] if x[-1] == '\n' else x, variables_raw))
        variables_raw = list(map(str.strip, variables_raw))
        variables_raw = list(filter(lambda x: x != '', variables_raw))

        for line in csv.reader(variables_raw, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True):
            assert len(line) == 2, 'Invalid CSV - Length is not 2'
            variables[line[0]] = line[1]

    result_message = message_template.format(**variables)

    try:
        bot = telebot.TeleBot(telegram_bot_token, parse_mode=None)
        bot.send_message(chat_id=telegram_bot_channel_id, text=result_message, timeout=5, disable_web_page_preview=True)
        bot.stop_bot()

        print('Telegram message - sent successfully!')
    except:
        print('Telegram message - ERROR')
        raise


# ======================================================================================================================

# OBS-related API functions


def script_properties():
    """
    Shows settings properties to a user
    """
    props = obs.obs_properties_create()

    # Common settings
    file_type = obs.OBS_PATH_FILE
    file_filter = "*.*"
    default_search_location = None

    # Add message property
    obs.obs_properties_add_path(props, CONSTANT_ID.source_name__message_path, "Message template file:",
                                file_type, file_filter, default_search_location)

    # Add variables property
    obs.obs_properties_add_path(props, CONSTANT_ID.source_name__variables_path, "Template variables file:",
                                file_type, file_filter, default_search_location)

    # Add telegram_bot_token password property
    obs.obs_properties_add_text(props, CONSTANT_ID.source_name__telegram_bot_token, "Telegram bot token:",
                                obs.OBS_TEXT_PASSWORD)

    # Add telegram_bot_channel_id property
    obs.obs_properties_add_text(props, CONSTANT_ID.source_name__telegram_bot_channel_id, "Telegram bot channel id:",
                                obs.OBS_TEXT_PASSWORD)

    # Add button for sending a test message
    obs.obs_properties_add_button(props, CONSTANT_ID.button_name__send_test_message, "Send a test message",
                                  send_notification)

    return props


def script_update(settings):
    """
    Called when the script’s settings (if any) have been changed by the user.
    """
    global message_template_file_path
    global template_variables_file_path
    global telegram_bot_token
    global telegram_bot_channel_id

    message_template_file_path = obs.obs_data_get_string(settings, CONSTANT_ID.source_name__message_path)
    template_variables_file_path = obs.obs_data_get_string(settings, CONSTANT_ID.source_name__variables_path)
    telegram_bot_token = obs.obs_data_get_string(settings, CONSTANT_ID.source_name__telegram_bot_token)
    telegram_bot_channel_id = obs.obs_data_get_string(settings, CONSTANT_ID.source_name__telegram_bot_channel_id)

    print('Reloaded property settings')


def on_event(event):
    if event == obs.OBS_FRONTEND_EVENT_STREAMING_STARTED:
        print("Streaming started")
        send_notification(None, None)
        return


def script_load(settings):
    obs.obs_frontend_add_event_callback(on_event)
